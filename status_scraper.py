import urllib.request as urllib2
import json
import datetime
import csv
import time

app_id = "1830595163894814"
app_secret = "4f42f0ce6594a0b9da459c0df97b1016"
access_token = app_id + "|" + app_secret

#page_id = "victor.ponta"
page_id ="klausiohannis"
start_date = datetime.datetime.strptime("2014-11-02T00:00:00+0000",'%Y-%m-%dT%H:%M:%S+0000')
stop_date = datetime.datetime.strptime("2014-11-17T00:00:00+0000",'%Y-%m-%dT%H:%M:%S+0000')

def request_until_succeed(url):
    req = urllib2.Request(url)
    success = False
    while success is False:
        try:
            response = urllib2.urlopen(req)
            if response.getcode() == 200:
                success = True
        except (Exception, e):
            print (e)
            time.sleep(5)

            print ("Error for URL %s: %s" % (url, datetime.datetime.now()))

    return response.read().decode()

def getFacebookPageFeedData(page_id, access_token, num_statuses):

    # construct the URL string
    base = "https://graph.facebook.com"
    node = "/" + page_id + "/feed"
    parameters = "/?fields=message,link,created_time,type,name,id,likes.limit(1).summary(true),comments.limit(1).summary(true),shares&limit=%s&access_token=%s" % (num_statuses, access_token) # changed
    url = base + node + parameters

    # retrieve data
    data = json.loads(request_until_succeed(url))

    return data

def processFacebookPageFeedStatus(status):

    # The status is now a Python dictionary, so for top-level items,
    # we can simply call the key.

    # Additionally, some items may not always exist,
    # so must check for existence first

    status_id = status['id']
    status_message = '' if 'message' not in status.keys() else status['message']
    link_name = '' if 'name' not in status.keys() else status['name'].encode('utf-8')
    status_type = status['type']
    status_link = '' if 'link' not in status.keys() else status['link']


    # Time needs special care since a) it's in UTC and
    # b) it's not easy to use in statistical programs.

    status_published = datetime.datetime.strptime(status['created_time'],'%Y-%m-%dT%H:%M:%S+0000')




    status_published = status_published + datetime.timedelta(hours=-5) # EST
    if status_published < start_date or status_published > stop_date:
        print ("not goood", status_published)
        return None
    else:
        print(status_published)

    status_published = status_published.strftime('%Y-%m-%d %H:%M:%S') # best time format for spreadsheet programs

    # Nested items require chaining dictionary keys.

    num_likes = 0 if 'likes' not in status.keys() else status['likes']['summary']['total_count']
    num_comments = 0 if 'comments' not in status.keys() else status['comments']['summary']['total_count']
    num_shares = 0 if 'shares' not in status.keys() else status['shares']['count']
    #print (status_type)
    #if status_type == "photo" or status_type == "video":
    #    return None
    # return a tuple of all processed data
    return (status_message, status_type,status_published, status_id)


def scrapeFacebookPageFeedStatus(page_id, access_token):
    with open('%s_facebook_statuses.csv' % page_id, 'w+') as file:
        w = csv.writer(file)
        w.writerow(["status_message", "status_type","status_published", "status_id"])

        has_next_page = True
        num_processed = 0   # keep a count on how many we've processed
        scrape_starttime = datetime.datetime.now()

       # print "Scraping %s Facebook Page: %s\n" % (page_id, scrape_starttime)

        statuses = getFacebookPageFeedData(page_id, access_token, 100)

        while has_next_page:
            for status in statuses['data']:
                processed_status = processFacebookPageFeedStatus(status)
                if not processed_status is None:
                	w.writerow(processed_status)

                # output progress occasionally to make sure code is not stalling
                num_processed += 1
                if num_processed % 100 == 0:
                        print(num_processed)

            # if there is no next page, we're done.
            if 'paging' in statuses.keys():
                statuses = json.loads(request_until_succeed(statuses['paging']['next']))
            else:
                has_next_page = False
                print ("No next page")


       # print ("\nDone!\n%s Statuses Processed in %s" % (num_processed, datetime.datetime.now() - scrape_starttime))
scrapeFacebookPageFeedStatus(page_id, access_token)

