import urllib.request as urllib2
import json
import datetime
import csv
import time

app_id = "1830595163894814"
app_secret = "4f42f0ce6594a0b9da459c0df97b1016"
access_token = app_id + "|" + app_secret

#page_id = "victor.ponta"
page_id ="klausiohannis"
start_date = datetime.datetime.strptime("2014-11-02T00:00:00+0000",'%Y-%m-%dT%H:%M:%S+0000')
stop_date = datetime.datetime.strptime("2019-11-17T00:00:00+0000",'%Y-%m-%dT%H:%M:%S+0000')

def request_until_succeed(url):
    req = urllib2.Request(url)
    success = False
    while success is False:
        try:
            response = urllib2.urlopen(req)
            if response.getcode() == 200:
                success = True
        except Exception as e:
            print (e)
            time.sleep(5)

            print ("Error for URL %s: %s" % (url, datetime.datetime.now()))

    return response.read().decode()

#https://graph.facebook.com/v2.10/1446622392091674_1446642522089661/?fields=comments.filter(stream).limit(1){message,id,from,like_count, parent}&access_token=1830595163894814|4f42f0ce6594a0b9da459c0df97b1016
def getFacebookPageFeedData(page_id, access_token, num_statuses):

    # construct the URL string
    base = "https://graph.facebook.com/v2.3"
    node = "/" + page_id + "/feed"
    parameters = "/?fields=message,link,created_time,type,name,id,likes.limit(1).summary(true),comments.limit(199).summary(true),shares&limit=%s&access_token=%s" % (num_statuses, access_token) # changed
    url = base + node + parameters

    # retrieve data
    data = json.loads(request_until_succeed(url))

    return data

def processFacebookPageFeedStatus(status):

    # The status is now a Python dictionary, so for top-level items,
    # we can simply call the key.

    # Additionally, some items may not always exist,
    # so must check for existence first

    status_id = status['id']
    status_message = '' if 'message' not in status.keys() else status['message']
    link_name = '' if 'name' not in status.keys() else status['name'].encode('utf-8')
    status_type = status['type']
    status_link = '' if 'link' not in status.keys() else status['link']


    # Time needs special care since a) it's in UTC and
    # b) it's not easy to use in statistical programs.

    status_published = datetime.datetime.strptime(status['created_time'],'%Y-%m-%dT%H:%M:%S+0000')




    status_published = status_published + datetime.timedelta(hours=-5) # EST
    if status_published < start_date or status_published > stop_date:
        print ("not goood", status_published)
        return []
    status_published = status_published.strftime('%Y-%m-%d %H:%M:%S') # best time format for spreadsheet programs

    # Nested items require chaining dictionary keys.

    num_likes = 0 if 'likes' not in status.keys() else status['likes']['summary']['total_count']
    num_comments = 0 if 'comments' not in status.keys() else status['comments']['summary']['total_count']
    num_shares = 0 if 'shares' not in status.keys() else status['shares']['count']


    has_next_page = True
    comments = status['comments']
    comment_list = []
    print(num_comments)
    while has_next_page:
        print(len(comment_list))
        has_next_page = False

        for comm_info in comments['data']:
            published_date = datetime.datetime.strptime(comm_info['created_time'],'%Y-%m-%dT%H:%M:%S+0000')
            published_date = published_date + datetime.timedelta(hours=-5) # EST
            published_date = published_date.strftime('%Y-%m-%d %H:%M:%S') # best time format for spreadsheet programs

            name = comm_info['from']['name']
            print ("Keys")
            print(comm_info.keys())
            if 'like_count' in comm_info:
                like_count = comm_info['like_count']
                print("Like count")
            if 'parent' in comm_info:
                parent = comm_info['parent']
                print("parent")

            comment_list.append((name, comm_info['message'], published_date, status_id))

        if 'paging' in comments.keys() and 'next' in comments['paging'].keys():
    	    has_next_page = True
    	    comments = json.loads(request_until_succeed(comments['paging']['next']))
        else:
            print ("No next page")

    #print (status_type)
    #if status_type == "photo" or status_type == "video":
    #    return None
    # return a tuple of all processed data
    return comment_list

#processed_test_status = processFacebookPageFeedStatus(test_status)
#print (processed_test_status)


def scrapeFacebookPageFeedComments(page_id, access_token):
    with open('%s_facebook_comments.csv' % page_id, 'w+') as file:
        w = csv.writer(file)
        w.writerow(["name", "message","date", "status_id"])

        has_next_page = True
        num_processed = 0   # keep a count on how many we've processed
        scrape_starttime = datetime.datetime.now()

        print ("Scraping %s Facebook Page: %s\n" % (page_id, scrape_starttime))

        statuses = getFacebookPageFeedData(page_id, access_token, 4)

        while has_next_page:
            for status in statuses['data']:
                # return a row of comments
                processed_status = processFacebookPageFeedStatus(status)
                for row in processed_status:
                    if not row is None:
                        w.writerow(row)

                # output progress occasionally to make sure code is not stalling
                num_processed += 1
                if num_processed % 100 == 0:
                        print(num_processed)

            # if there is no next page, we're done.
            if 'paging' in statuses.keys():
                statuses = json.loads(request_until_succeed(statuses['paging']['next']))
            else:
                has_next_page = False
                print ("No next page")


       # print ("\nDone!\n%s Statuses Processed in %s" % (num_processed, datetime.datetime.now() - scrape_starttime))
scrapeFacebookPageFeedComments(page_id, access_token)
